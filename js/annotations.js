/*Final code that removes the Annotations on youtube*/
$('.video-annotations').css('display','none');
$('.annotation').css('display','none');

console.log(' ');
console.log('%cThank You for installing the No Annotations Plugin - <PolyDev.co.za>', 'color: #f90; font-size: 15px');
console.log('%cNo Annotations Beta Plugin only activates when you visit YouTube and removes unneeded video-annotations that block video view and needs to be closed','color:#7986cb;font-size: 14px');
console.log('%cDeveloped by Torean Joel', 'color: #3BA977;font-size: 13px');
console.log(' ');

